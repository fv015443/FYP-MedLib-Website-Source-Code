<?php

if (isset($_POST['login-submit'])) {
    require 'dbh.inc.php';

    $mailuid = $_POST['mailuid'];
    $password = $_POST['pwd'];

    if (empty($mailuid) || empty($password)) {
        ?>
        <script type="text/javascript">
            alert("Username or Password empty");
            history.back();
        </script>
        <?php
    }
    else {
        $sql = "SELECT * FROM users WHERE uidUsers=? OR emailUsers=?;";
        $stmt = mysqli_stmt_init($conn2);
        if (!mysqli_stmt_prepare($stmt, $sql)) {
            header("Location: ../login.php");
            exit();

        }
        else {
            mysqli_stmt_bind_param($stmt, "ss", $mailuid, $mailuid);
            mysqli_stmt_execute($stmt);
            $result = mysqli_stmt_get_result($stmt);
            if ($row = mysqli_fetch_assoc($result)){
                $pwdCheck = password_verify($password, $row['pwdUsers']);

                if ($pwdCheck == true) {
                    session_start();
                    $_SESSION['userId'] = $row['idUsers'];
                    $_SESSION['userUid'] = $row['uidUsers'];
                    $_SESSION['userType'] = $row['typeUsers'];
                    if($_SESSION['userType']=="Admin")
                        header("Location: ../adminHome.php");
                    else
                        header("Location: ../index.php");
                    exit();

                }
                else {
                     ?>
                <script type="text/javascript">
                    alert("Incorrect Password");
                    history.back();
                </script>
                <?php
                }
            }
            else {
                 ?>
                <script type="text/javascript">
                    alert("Invalid Username");
                    history.back();
                </script>
                <?php
            }
        }
    }

}
else {
    header("Location: ../login.php");
    exit();
}