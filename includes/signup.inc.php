<?php

if (isset($_POST['signup-submit'])) {

    require 'dbh.inc.php';

    $username = $_POST['uid'];
    $email = $_POST['mail'];
    $password = $_POST['pwd'];
    $passwordRepeat = $_POST['pwd-repeat'];

    if (empty($username) || empty($email) || empty($password) || empty($passwordRepeat)) {
        ?>
        <script type="text/javascript">
            alert("Username or Password empty");
            history.back();
        </script>
        <?php

    }
    else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
       ?>
        <script type="text/javascript">
            alert("Invalid Email");
            history.back();
        </script>
        <?php
    }
    else if (!preg_match("/^[a-zA-Z0-9]*$/", $username)) {
        ?>
        <script type="text/javascript">
            alert("Invalid ID");
            history.back();
        </script>
        <?php
    }
    else if ($password !== $passwordRepeat) {
        ?>
        <script type="text/javascript">
            alert("Passwords does not match");
            history.back();
        </script>
        <?php
    }
    else {
        $sql2 = "SELECT uidUsers FROM users WHERE uidUsers=?";
        $stmt = mysqli_stmt_init($conn2);
        if (!mysqli_stmt_prepare($stmt, $sql2)) {
            header("Location: ../signup.php?error=sqlerror");
            exit();

        }
        else {
            mysqli_stmt_bind_param($stmt, "s", $username);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_store_result($stmt);
            $resultCheck = mysqli_stmt_num_rows($stmt);

            if($resultCheck > 0) {
                ?>
                <script type="text/javascript">
                    alert("This Username is already taken. Please choose another one");
                    history.back();
                </script>
                <?php
            }
            else {
                $sql2 = "INSERT INTO users (uidUsers, emailUsers, pwdUsers,typeUsers) VALUES (?, ?, ?,?)";
                $stmt = mysqli_stmt_init($conn2);
                if (!mysqli_stmt_prepare($stmt, $sql2)) {
                    header("Location: ../signup.php?error=sqlerror");
                    exit();
            }
            else {
                $hashedPwd = password_hash($password, PASSWORD_DEFAULT);
                $type="User";
                mysqli_stmt_bind_param($stmt, "ssss", $username, $email, $hashedPwd,$type);
                mysqli_stmt_execute($stmt);
                ?>
                <script type="text/javascript">
                    alert("Successfully Registered");
                    window.location="../login.php";
                </script>
                <?php
                //header("Location: ../loginpage.php");
            }
        }
    }
    mysqli_stmt_close($stmt);
}
    
    mysqli_close($conn2);

}
    else {
    //header("Location: ../signup.php");
    exit();
}