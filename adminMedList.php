<?php include 'header.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Med List</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
<link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/animate.css">
<link rel="stylesheet" type="text/css" href="styles/roof.css">
<link rel="stylesheet" type="text/css" href="styles/roof_responsive.css">
<link rel="stylesheet" type="text/css" href="styles/news.css">
<link rel="stylesheet" type="text/css" href="styles/news_responsive.css">
<style>
a.delete {
  color: red;
}
</style>
</head>
<body>

<div class="super_container">

	<!-- Menu -->

	<div class="menu trans_500">
		<div class="menu_content d-flex flex-column align-items-center justify-content-center text-center">
			<div class="menu_close_container"><div class="menu_close"></div></div>
			<form action="#" class="menu_search_form">
				<input type="text" class="menu_search_input" placeholder="Search" required="required">
				<button class="menu_search_button"><i class="fa fa-search" aria-hidden="true"></i></button>
			</form>
			<ul>
				<li class="menu_item"><a href="adminHome.php">Home</a></li>
				<li class="menu_item"><a href="adminHCList.php">Health Conditions</a></li>
				<li class="menu_item"><a href="adminMedList.php">Medicines</a></li>
				<li class="menu_item"><a href="addHC.php">Add Health Conditions</a></li>
				<li class="menu_item"><a href="addMed.php">Add Medicines</a></li>	
				<li class="menu_item"><a href="includes/logout.inc.php">Logout</a></li>	
			</ul>
		</div>
		<div class="menu_social">
			<ul>
				<li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-behance" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
			</ul>
		</div>
	</div>
	
	<!-- Home -->

	<div class="home">
		<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="images/med.jpg" data-speed="0.8"></div>

	<header class="header" id="header">
			<div>
				<div class="header_top">
					<div class="container">
						<div class="row">
							<div class="col">
								<div class="header_top_content d-flex flex-row align-items-center justify-content-start">
									<div class="logo">
										<a href="#"><span>MEDLIB</span></a>	
									</div>
									
									
									<div class="hamburger ml-auto"><i class="fa fa-bars" aria-hidden="true"></i></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="header_nav" id="header_nav_pin">
					<div class="header_nav_inner">
						<div class="header_nav_container">
							<div class="container">
								<div class="row">
									<div class="col">
										<div class="header_nav_content d-flex flex-row align-items-center justify-content-start">
											<nav class="main_nav">
												<ul class="d-flex flex-row align-items-center justify-content-start">	
													<li><a href="adminHome.php">Home</a></li>
													<li><a href="adminHCList.php">Health Conditions</a></li>
													<li><a href="adminMedList.php">Medicines</a></li>
													<li><a href="addHC.php">Add Health Conditions</a></li>
													<li><a href="addMed.php">Add Medicines</a></li>
													<li><a href="includes/logout.inc.php">Logout</a></li>
												</ul>
											</nav>	
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</header>

		<div class="home_container">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="home_content">
							<div class="home_title">Medicines List</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="news">
		<div class="container">
			<div class="row">
				<div class="col text-center">
					<div class="section_title">All Medicines A - Z</div>
					<div class="section_subtitle">Here you can see all of the medicines on the website</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3"></div>
				<div class="col-lg-6">
					<div class="news_sidebar">

						<div class="sidebar_categories">

							<div class="categories">
								<div align="right"><button onclick="window.location.href = 'addMed.php';"class="footer_contact_button">Add</button></div>
								<table class="table .table-striped table-hover table-bordered" border="1">
									<thead>
										<tr>
											<th>Medicine</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<?php
										global $conn;
										$sql = "SELECT * FROM medicines ORDER BY D_Name ASC";
										$result = mysqli_query($conn, $sql);
										$queryResult = mysqli_num_rows($result);
										echo "$queryResult Records Found";
										if ($queryResult > 0) {
											while ($row = mysqli_fetch_assoc($result)) {
											?>
												<tr>
													<td><?php echo $row['D_Name']; ?></td>
													<td><a href=<?php echo "editMed.php?id=".$row['Id'];?> >Edit</a> <a class="delete" onclick="return confirm('Are you sure you want delete this medicine?')" href=<?php echo "deleteMed.php?id=".$row['Id'];?> >   Delete</a></td>
												</tr>
											<?php
											}
										}
										else{
											?>
											<tr><td colspan="2">No Medicines listed</td></tr>
											<?php
										
										}
										?>


									</tbody>
									
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php  include("footer.php"); ?>
</div>

<script src="js/jquery-3.3.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/greensock/TweenMax.min.js"></script>
<script src="plugins/greensock/TimelineMax.min.js"></script>
<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="plugins/greensock/animation.gsap.min.js"></script>
<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="js/roof.js"></script>
</body>
</html>