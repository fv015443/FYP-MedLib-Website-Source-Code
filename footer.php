<!-- Footer -->

	<footer class="footer">
		<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="images/footer2.jpg" data-speed="0.8"></div>
		<div class="footer_content">
			<div class="container">
				<div class="row">

					<!-- Footer About -->
					<div class="col-lg-5 footer_col">
						<div class="footer_about">
							<div class="logo">
								<a href="#">medlib</a>	
							</div>
							<div class="footer_about_text">MedLib is a search tool for pharmaceuticals providing information on Medicine and Drugs used primarily for the UK.</div>
							
							
						</div>
					</div>
					
					<!-- Footer Contact -->
					<div class="col-lg-3 footer_col">
						<ul class="hours_list">
								<li class="d-flex flex-row align-items-center justify-content-start">
									<a href="index.php"><div>Home</div></a>
								</li>
								<li class="d-flex flex-row align-items-center justify-content-start">
									<a href="healthConditionList.php"><div>Health Conditions</div></a>
								</li>
								<li class="d-flex flex-row align-items-center justify-content-start">
									<a href="medicineList.php"><div>Medicines</div></a>
								</li>
								<li class="d-flex flex-row align-items-center justify-content-start">
									<a href="nearMe.php"><div>Pharmacy Near Me</div></a>
								</li>
								<li class="d-flex flex-row align-items-center justify-content-start">
									<a href="profile.php"><div>Profile</div></a>
								</li>
								<li class="d-flex flex-row align-items-center justify-content-start">
									<a href="aboutUs.php"><div>About Us</div></a>
								</li>
							</ul>
					</div>

					<!-- Footer Hours -->
					<div class="col-lg-4 footer_col">
						<div class="footer_social">
								<ul class="d-flex flex-row align-items-center justify-content-start">
									<li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
									<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
									<li><a href="#"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
									<li><a href="#"><i class="fa fa-behance" aria-hidden="true"></i></a></li>
									<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
								</ul>
							</div>
							
							
						
					</div>
				</div>
			</div>
		</div>
	
	</footer>