<?php include 'header.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
<title>About us</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Health medical template project">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
<link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/animate.css">
<link rel="stylesheet" type="text/css" href="styles/about.css">
<link rel="stylesheet" type="text/css" href="styles/about_responsive.css">
</head>
<body>

<div class="super_container">

	<!-- Menu -->

	<div class="menu trans_500">
		<div class="menu_content d-flex flex-column align-items-center justify-content-center text-center">
			<div class="menu_close_container"><div class="menu_close"></div></div>
			<ul>
				<li class="menu_item"><a href="index.php">Home</a></li>
		        <li class="menu_item"><a href="nearMe.php">Pharmacy Near Me</a></li>
		        <li class="menu_item"><a href="healthConditionList.php">All Health Conditions</a></li>
		        <li class="menu_item"><a href="medicineList.php">All Medicines</a></li>
		        <li class="menu_item"><a href="aboutUs.php">About Us</a></li>
		        <li class="menu_item"><a href="profile.php">Profile</a></li>  
		        <li class="menu_item"><a href="includes/logout.inc.php">Logout</a></li> 
			</ul>
		</div>
		<div class="menu_social">
			<ul>
				<li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-behance" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
			</ul>
		</div>
	</div>
	
	<!-- Home -->

	<div class="home">
		<!-- <div class="background_image" style="background-image:url(images/about.jpg)"></div> -->
		<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="images/about.jpg" data-speed="0.8"></div>

		<!-- Header -->

		<header class="header" id="header">
			<div>
				<div class="header_top">
					<div class="container">
						<div class="row">
							<div class="col">
								<div class="header_top_content d-flex flex-row align-items-center justify-content-start">
									<div class="logo">
										<a href="index.php"><span>MEDLIB</span></a>	
									</div>
									
									
									<div class="hamburger ml-auto"><i class="fa fa-bars" aria-hidden="true"></i></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="header_nav" id="header_nav_pin">
					<div class="header_nav_inner">
						<div class="header_nav_container">
							<div class="container">
								<div class="row">
									<div class="col">
										<div class="header_nav_content d-flex flex-row align-items-center justify-content-start">
											<nav class="main_nav">
												<ul class="d-flex flex-row align-items-center justify-content-start">
													<li><a href="index.php">Home</a></li>
							                        <li><a href="healthConditionList.php">All Health Conditions</a></li>
							                        <li><a href="medicineList.php">All Medicines</a></li>
							                        <li><a href="nearMe.php">Pharmacy Near Me</a></li>
							                        <li><a href="profile.php">Profile</a></li>
							                        <li><a href="aboutUs.php">About Us</a></li>
							                        <li><a href="includes/logout.inc.php">Logout</a></li>
												</ul>
											</nav>
											
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</header>

		<div class="home_container">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="home_content">
							<div class="home_title">About us</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- About -->

	<div class="about">
		<div class="container">
			<div class="row">
				<div class="col text-center">
					<div class="section_title">A few words about us</div>
				</div>
			</div>
			<div class="row about_row row-eq-height">
				<div class="col-lg-4">
					<div class="logo">
						<a href="#"><span>MEDLIB</span></a>	
					</div>
					<div class="about_text_highlight">MedLib is a search tool for pharmaceuticals providing information on Medicine and Drugs used primarily for the UK.</div>
					<div class="about_text">
						<p>We believe that people that use or have used medicine and drugs deserve to know what they take.</p>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="about_text_2">
						<p>They shouldn't just take the word of Pharmacisits, Chemist or Doctors and take what they think is right. Thats why this website was created, to give users information on what they take such as recommended dosages, detailed information about the medicine and also the different forms that the medicine can be taken in.</p>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="about_image"><img src="images/pill.jpg" alt=""></div>
				</div>
			</div>
		</div>
	</div>

	<div class="milestones">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 milestone_col"></div>
				<?php 	
				$stmt = $conn->prepare("SELECT COUNT(Id) FROM medicines ");
                $stmt->execute();
                $stmt->store_result();
                $stmt->bind_result($med_count);
                $stmt->fetch(); 
                $stmt->close();

                $stmt = $conn->prepare("SELECT COUNT(HC_id) FROM health_conditions ");
                $stmt->execute();
                $stmt->store_result();
                $stmt->bind_result($hc_count);
                $stmt->fetch(); 
                $stmt->close();
                ?>
				<div class="col-lg-3 milestone_col">
					<div class="milestone d-flex flex-row align-items-center justify-content-start">
						<div class="milestone_icon d-flex flex-column align-items-center justify-content-center"><img src="images/icon_6.svg" alt=""></div>
						<div class="milestone_content">
							<div class="milestone_counter" data-end-value="<?php echo $med_count;?>" ><?php echo $med_count;?></div>
							<div class="milestone_text">Medicines</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 milestone_col">
					<div class="milestone d-flex flex-row align-items-center justify-content-start">
						<div class="milestone_icon d-flex flex-column align-items-center justify-content-center"><img src="images/icon_8.svg" alt=""></div>
						<div class="milestone_content">
							<div class="milestone_counter" data-end-value="<?php echo $hc_count;?>"><?php echo $hc_count;?></div>
							<div class="milestone_text">Health Conditions</div>
						</div>
						
					</div>
				</div>
				<div class="col-lg-3 milestone_col"></div>
			</div>
		</div>
	</div>
	<div class="about" style="background-color: #CEF2E8">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 footer_col"></div>
				<div class="col-lg-5 footer_col">
					<div class="footer_contact">
						<div class="footer_contact_title" style="font-size: 24px;color: black;font-weight: bold">Quick Contact</div>
						<div class="footer_contact_form_container">
							<form action="sendMessage.php" class="footer_contact_form" id="footer_contact_form" method="POST">
								<div class="d-flex flex-xl-row flex-column align-items-center justify-content-between">
									<input type="text" class="footer_contact_input" placeholder="Name" name="name" required="required">
									<input type="email" class="footer_contact_input" placeholder="E-mail" name="email" required="required">
								</div>
								<textarea class="footer_contact_input footer_contact_textarea" name="message" placeholder="Message" required="required"></textarea>
								<button class="footer_contact_button" name="msgButton">send message</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php  include("footer.php"); ?>
</div>

<script src="js/jquery-3.3.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/greensock/TweenMax.min.js"></script>
<script src="plugins/greensock/TimelineMax.min.js"></script>
<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="plugins/greensock/animation.gsap.min.js"></script>
<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="js/about.js"></script>
</body>
</html>