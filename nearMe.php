<?php include 'header.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Near Me</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
<link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="styles/contact.css">
<link rel="stylesheet" type="text/css" href="styles/contact_responsive.css">
</head>
<body>

<div class="super_container">

	<!-- Menu -->

	<div class="menu trans_500">
		<div class="menu_content d-flex flex-column align-items-center justify-content-center text-center">
			<div class="menu_close_container"><div class="menu_close"></div></div>
			<form action="#" class="menu_search_form">
				<input type="text" class="menu_search_input" placeholder="Search" required="required">
				<button class="menu_search_button"><i class="fa fa-search" aria-hidden="true"></i></button>
			</form>
			<ul>
				<li class="menu_item"><a href="index.php">Home</a></li>
		        <li class="menu_item"><a href="nearMe.php">Pharmacy Near Me</a></li>
		        <li class="menu_item"><a href="healthConditionList.php">All Health Conditions</a></li>
		        <li class="menu_item"><a href="medicineList.php">All Medicines</a></li>
		        <li class="menu_item"><a href="aboutUs.php">About Us</a></li>
		        <li class="menu_item"><a href="profile.php">Profile</a></li>  
		        <li class="menu_item"><a href="includes/logout.inc.php">Logout</a></li> 
			</ul>
		</div>
		<div class="menu_social">
			<ul>
				<li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-behance" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
			</ul>
		</div>
	</div>
	
	<!-- Home -->

	<div class="home">
		<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="images/contact.jpg" data-speed="0.8"></div>

		<!-- Header -->

			<header class="header" id="header">
			<div>
				<div class="header_top">
					<div class="container">
						<div class="row">
							<div class="col">
								<div class="header_top_content d-flex flex-row align-items-center justify-content-start">
									<div class="logo">
										<a href="index.php"><span>MEDLIB</span></a>	
									</div>
									<div class="hamburger ml-auto"><i class="fa fa-bars" aria-hidden="true"></i></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="header_nav" id="header_nav_pin">
					<div class="header_nav_inner">
						<div class="header_nav_container">
							<div class="container">
								<div class="row">
									<div class="col">
										<div class="header_nav_content d-flex flex-row align-items-center justify-content-start">
											<nav class="main_nav">
												<ul class="d-flex flex-row align-items-center justify-content-start">
													<li><a href="index.php">Home</a></li>
							                        <li><a href="healthConditionList.php">All Health Conditions</a></li>
							                        <li><a href="medicineList.php">All Medicines</a></li>
							                        <li><a href="nearMe.php">Pharmacy Near Me</a></li>
							                        <li><a href="profile.php">Profile</a></li>
							                        <li><a href="aboutUs.php">About Us</a></li>
							                        <li><a href="includes/logout.inc.php">Logout</a></li>	
												</ul>
											</nav>
											
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</header>
		<div class="home_container">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="home_content">
							<div class="home_title">Pharmacies Near You</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="contact_map">
		<div class="container">
			<div class="row">
				<div class="contact_title">Pharmacies Near You</div>
				<div class="contact_form_container">
					<p  style="font-weight: bold">Use the below map to find pharmacies near you. You can use your mouse to drag the map to discover more and more pharmacies around your area.</p>
				</div>
			</div>
		</div>
		<div class="map">
			<div id="google_map" class="google_map">
				<div class="map_container">
					<div class="map" style="background: #82E0AA; text-align: center;">
						<iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d19896.972640614742!2d-0.9569209575428311!3d51.43756104180346!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1spharmacy!5e0!3m2!1sen!2suk!4v1580738697698!5m2!1sen!2suk" width="100%" height="100%" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php  include("footer.php"); ?>
</div>

<script src="js/jquery-3.3.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCIwF204lFZg1y4kPSIhKaHEXMLYxxuMhA"></script>
<script src="js/contact.js"></script>
</body>
</html>