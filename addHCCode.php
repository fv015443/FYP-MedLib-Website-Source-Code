<?php	
if (isset($_POST['msgButton'])) {
	require 'includes/dbh.inc.php';
	$hcname=$_POST['hcname'];
	$symptom=$_POST['symptom'];
	$description=$_POST['description'];
	$treatment=$_POST['treatment'];
	$sql = "INSERT INTO health_conditions (HC_Name, HC_Descrip, HC_Symptom,HC_Medicine) VALUES (?, ?, ?, ?)";
    $stmt = mysqli_stmt_init($conn);
    mysqli_stmt_prepare($stmt, $sql);
    mysqli_stmt_bind_param($stmt, "ssss", $hcname, $description, $symptom,$treatment);
    mysqli_stmt_execute($stmt);
    ?>
    <script type="text/javascript">
        alert("Health Condition Added");
        window.location="adminHCList.php";
    </script>
<?php
}
?>