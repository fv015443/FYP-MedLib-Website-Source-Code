-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 14, 2020 at 03:53 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.3.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `loginsystem`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `idUsers` int(11) NOT NULL,
  `uidUsers` tinytext NOT NULL,
  `emailUsers` tinytext NOT NULL,
  `pwdUsers` longtext NOT NULL,
  `typeUsers` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`idUsers`, `uidUsers`, `emailUsers`, `pwdUsers`, `typeUsers`) VALUES
(6, 'test123', 'test123@123.com', '$2y$10$/9nABL/i04Xamg69ClU5O.5nJfjC.VqA4WQvjtkOTOwBLQICssH1m', 'User'),
(8, 'admin', 'hassanshakeel214@gmail.com', '$2y$10$Rui3rVoh7nN66bbes6c9v.q0ezg2Cm2c/cwLLoaYRHefGAVZX.YZO', 'Admin'),
(9, 'test111', 'test111@gmail.com', '$2y$10$i/.ART0vN.B8bS0JePKna.Zml.dIQRdMZaMCrnXyXxLMxI2.rAq/W', 'User'),
(10, 'hassan', 'hassan@gmail.com', '$2y$10$u6jSh21sYy7jMxY2ubA/v.ky4MJ.68C6ypJhSeozdKRyxaou4msle', 'User'),
(11, 'Hassannew', 'hassannew@gmail.com', '$2y$10$5vDa3BPUpE96we7jBHBrpeW73DTCTrNC/jfFmTCTwvCbeKTK80iL.', 'User');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`idUsers`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `idUsers` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
