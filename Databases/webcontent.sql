-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 14, 2020 at 03:53 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.3.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `webcontent`
--

-- --------------------------------------------------------

--
-- Table structure for table `health_conditions`
--

CREATE TABLE `health_conditions` (
  `HC_id` int(11) NOT NULL,
  `HC_Name` text NOT NULL,
  `HC_Descrip` longtext NOT NULL,
  `HC_Symptom` longtext NOT NULL,
  `HC_Medicine` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `health_conditions`
--

INSERT INTO `health_conditions` (`HC_id`, `HC_Name`, `HC_Descrip`, `HC_Symptom`, `HC_Medicine`) VALUES
(1, 'ADHD - Attention Deficit Hyperactive Disorder', 'Attention deficit hyperactivity disorder (ADHD) is a behavioural disorder that includes symptoms such as inattentiveness, hyperactivity and impulsiveness.\r\n\r\nSymptoms of ADHD tend to be noticed at an early age and may become more noticeable when a child\'s circumstances change, such as when they start school.\r\n\r\nMost cases are diagnosed when children are 6 to 12 years old.\r\n\r\nThe symptoms of ADHD usually improve with age, but many adults who were diagnosed with the condition at a young age continue to experience problems.\r\n\r\nPeople with ADHD may also have additional problems, such as sleep and anxiety disorders.', 'The symptoms of attention deficit hyperactivity disorder (ADHD) can be categorised into 2 types of behavioural problems: inattentiveness, and hyperactivity and impulsiveness.\r\n\r\nMost people with ADHD have problems that fall into both these categories, but this is not always the case.\r\n\r\nFor example, some people with the condition may have problems with inattentiveness, but not with hyperactivity or impulsiveness.\r\n\r\nThis form of ADHD is also known as attention deficit disorder (ADD). ADD can sometimes go unnoticed because the symptoms may be less obvious.', 'There are 5 types of medication licensed for the treatment of ADHD:\r\n\r\nMethylphenidate \r\nDexamfetamine\r\nLisdexamfetamine\r\nAtomoxetine\r\nGuanfacine'),
(2, 'High Cholesterol', 'High cholesterol is when you have too much of a fatty substance called cholesterol in your blood\r\nIt\'s mainly caused by eating fatty food, not exercising enough, being overweight, smoking and drinking alcohol. It can also run in families\r\nyou can lower your cholesterol by eating healthily and getting more exercise. Some people also need to take medicine\r\nToo much cholesterol can block your blood vessels. It makes you more likely to have heart problems or a stroke\r\nHigh cholesterol does not cause symptoms. You can only find out if you have it from a blood test', 'High cholesterol has no symptoms. A blood test is the only way to detect if you have it.\r\n\r\nWhen to see a doctor:\r\nAsk your doctor if you should have a cholesterol test. Children and young adults with no risk factors for heart disease are usually tested once between the ages of 9 and 11 and again between the ages of 17 and 19. Retesting for adults with no risk factors for heart disease is usually done every five years.\r\n\r\nIf your test results aren\'t within desirable ranges, your doctor might recommend more-frequent measurements. Your doctor might also suggest more-frequent tests if you have a family history of high cholesterol, heart disease or other risk factors, such as smoking, diabetes or high blood pressure.', 'Statins are the most common medicine for high cholesterol.\r\nThey reduce the amount of cholesterol your body makes.\r\nYou take a tablet once a day. You usually need to take them for life.'),
(3, 'Type 2 Diabetes', 'Type 2 diabetes is a lifelong disease that keeps your body from using insulin the way it should. People with type 2 diabetes are said to have insulin resistance.\r\n\r\nPeople who are middle-aged or older are most likely to get this kind of diabetes, so it used to be called adult-onset diabetes. But type 2 diabetes also affects kids and teens, mainly because of childhood obesity.\r\n\r\nIt’s the most common type of diabetes. There are about 29 million people in the U.S. with type 2. Another 84 million have prediabetes, meaning their blood sugar (or blood glucose) is high but not high enough to be diabetes yet.', 'Being very thirsty\r\nPeeing a lot\r\nBlurry vision\r\nBeing cranky\r\nTingling or numbness in your hands or feet\r\nFatigue/feeling worn out\r\nWounds that don\'t heal\r\nYeast infections that keep coming back\r\nHunger\r\nWeight loss without trying\r\nGetting more infections', 'Weight Loss\r\nHealthy Eating\r\nExercise\r\nTracking Blood Sugar Levels'),
(4, 'Cold, Flu & Cough', 'The common cold, including chest cold and head cold, and seasonal flu are caused by viruses. Use over-the-counter cold medications to relieve symptoms including sore throat, runny nose, congestion, and cough. Flu symptoms are similar, but include fever, headache and muscle soreness.', 'Fever\r\nSore Throat\r\nCough\r\nRunny Nose\r\nCongestion\r\nHeadache\r\nMuscle Pain', 'Over the counter medication for colds and fevers.\r\n\r\nLozenges\r\nTablets\r\nCough Sweets\r\nSyrups'),
(5, 'Heartburn/GERD', 'Heartburn or acid reflux symptoms include chronic cough and chest pain and burning. Knowing your triggers, such as certain foods, medications, obesity, or even stress, can help prevent heartburn. Heartburn treatment may include medications, home remedies, or diet changes.', 'Stress\r\nChest Pain\r\nBurning Sensation in chest\r\nIndigestion', 'Diet Changes\r\nOver the counter medicines\r\nHome Remedies'),
(6, 'Arthritis', 'Common arthritis symptoms of pain and stiffness are usually caused by degenerative arthritis (osteoarthritis). The more than 100 types of arthritis include rheumatoid arthritis and gout. A diagnosis is necessary in order to develop a treatment plan.', 'Pain in joints\r\nStiffness\r\nPain when in one positions for a long period of time\r\nPain when moving\r\nMuscle Pain', 'Pain Killers\r\nAnti-Inflammatory Drugs (Non-Steroidal)\r\nCounterirritants\r\nCorticosteroids\r\n \r\n'),
(7, 'Depression', 'Most people go through periods of feeling down, but when you\'re depressed you feel persistently sad for weeks or months, rather than just a few days.\r\n\r\nSome people think depression is trivial and not a genuine health condition. They\'re wrong – it is a real illness with real symptoms. Depression is not a sign of weakness or something you can \"snap out of\" by \"pulling yourself together\".\r\n', 'Continuous low mood or sadness\r\nFeeling hopeless and helpless\r\nHaving low self-esteem \r\nFeeling tearful\r\nFeeling guilt-ridden\r\nFeeling irritable and intolerant of others \r\nHaving no motivation or interest in things\r\nFinding it difficult to make decisions\r\nNot getting any enjoyment out of life\r\nFeeling anxious or worried \r\nHaving suicidal thoughts or thoughts of harming yourself', 'Antidepressents\r\nCombination Therapy\r\nPsychologists\r\nPsychiatrists\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `medicines`
--

CREATE TABLE `medicines` (
  `Id` int(255) NOT NULL,
  `D_Name` char(255) NOT NULL,
  `D_Type` char(255) NOT NULL,
  `D_Dosage` varchar(999) NOT NULL,
  `D_Description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `medicines`
--

INSERT INTO `medicines` (`Id`, `D_Name`, `D_Type`, `D_Dosage`, `D_Description`) VALUES
(1, 'Amoxicillin', 'Oral', 'Dosage based on directions of your doctor. Usually consumed within one hour of finishing a meal, usually once a day.', 'Amoxicillin is used to treat bacterial infections. It is a penicillin-type antibiotic. Works by stopping the growth of bacteria. Does not work for viral infections, just bacterial infection. Using Amoxicillin when it\'s not needed may cause it not to work in future bacterial infections. '),
(2, 'Domperidone', 'Tablets and Oral Solutions', 'Usual dose is 10mg taken up to three times a day if needed. Doses should be split evenly and consumed at least 8 hours apart. ', 'Domperidone is a anti-sickness medication that helps stop feeling or being sick. It is also used to treat stomach pain if the patient is on end of life care. Domperidone is also sometimes used to increase milk supply if the patient is finding it hard to breastfeed. '),
(3, 'Dulcolax', 'Oral tablets or oral solution', '5 to 15 mg (1 to 3 tablets) orally once a day as needed\r\n', 'Dulcolax is a laxative that simulates bowel movements. It is used to treat constipation or to empty the bowels before surgery or other intestinal medical procedures. Generally produces bowel movements within 6 to 12 hours. '),
(4, 'Cetirizine', 'Oral tablets or oral solutions', '10mg once a day', 'Cetirizine is a antihistamine that is used to relief allergy symptoms. It is used to treat hay fever, conjunctivitis, eczema, hives, reactions to insect bites and stings and some food allergies. It is non-drowsy and is less likely to make you feel sleepy. '),
(5, 'Tramadol', 'Oral tablets, capsules, drops. Injections (usually used in hospitals).', 'The dosage varies from person to person. It depends on how much pain you\'re in or if you\'ve had reactions or side effects from previous pain killers. ', 'Tramadol is used to treat moderate to severe pain. It is used commonly after surgery or a serious injury. It is available only on prescription. It works by blocking pain signals from travelling along the nerves of the brain.'),
(6, 'Vimovo', 'Oral Tablets and Capsules.', 'Doses depend on doctors instructions', 'Vimovo contains naproxen which is a non steroidal anti inflammatory drug. it works by reducing substances in the body that cause inflammation, pain and fever. Vimovo also contains Esomeprazole which is a proton pump inhibitor. It reduces the amount of acid produce in the stomach. '),
(7, 'Lisinopril', 'Oral Tablet', 'Initially 10 mg once daily; usual maintenance 20 mg once daily; maximum 80 mg per day.', 'Lisinopril is an ACE inhibitor. ACE stands for angiotensin converting enzyme.\r\n\r\nLisinopril is used to treat high blood pressure (hypertension) in adults and children who are at least 6 years old.\r\n\r\nLisinopril is also used to treat congestive heart failure in adults, or to improve survival after a heart attack.'),
(8, 'Diclofenac', 'Cream or Oral Tablet', 'Diclofenac free acid capsules: 35 mg orally 3 times a day\r\nDiclofenac potassium immediate-release tablets: 50 mg orally 2 or 3 times a day\r\nDiclofenac sodium enteric-coated tablets: 50 mg orally 2 or 3 times a day or 75 mg orally 2 times a day\r\nMaximum dose: 150 mg daily', 'Diclofenac is a nonsteroidal anti-inflammatory drug (NSAID). This medicine works by reducing substances in the body that cause pain and inflammation.\r\n\r\nDiclofenac is used to treat mild to moderate pain, or signs and symptoms of osteoarthritis or rheumatoid arthritis. Voltaren is also indicated for the treatment of ankylosing spondylitis.The Cataflam brand of this medicine is also used to treat menstrual cramps.\r\n\r\nDiclofenac powder (Cambia) is used to treat a migraine headache attack. Cambia will only treat a headache that has already begun. It will not prevent headaches or reduce the number of attacks.'),
(9, 'Adderall', 'Oral Tablet', 'Not recommended for children under 3 years of age. In children from 3 to 5 years of age, start with 2.5 mg daily; daily dosage may be raised in increments of 2.5 mg at weekly intervals until optimal response is obtained.\r\n\r\nIn children 6 years of age and older, start with 5 mg once or twice daily; daily dosage may be raised in increments of 5 mg at weekly intervals until optimal response is obtained. Only in rare cases will it be necessary to exceed a total of 40 mg per day. Give first dose on awakening; additional doses (1 or 2) at intervals of 4 to 6 hours.\r\n\r\nWhere possible, drug administration should be interrupted occasionally to determine if there is a recurrence of behavioral symptoms sufficient to require continued therapy.', 'Adderall contains a combination of amphetamine and dextroamphetamine. Amphetamine and dextroamphetamine are central nervous system stimulants that affect chemicals in the brain and nerves that contribute to hyperactivity and impulse control.\r\n\r\nAdderall is used to treat attention deficit hyperactivity disorder (ADHD) and narcolepsy.\r\n\r\nAdderall may also be used for purposes not listed in this medication guide.'),
(10, 'Antivert', 'Oral Tablet', 'The recommended dosage is 25 mg to 100 mg daily administered orally, in divided doses, depending upon clinical response.', 'Antivert (meclizine) is an antihistamine that reduces the effects of natural chemical histamine in the body.\r\n\r\nAntivert is used to treat or prevent nausea, vomiting, and dizziness caused by motion sickness.\r\n\r\nAntivert is also used to treat symptoms of vertigo (dizziness or spinning sensation) caused by disease that affects your inner ear.'),
(11, 'Avapro', 'Oral Tablet', 'The recommended initial dose of AVAPRO is 150 mg once daily. The dosage can be increased to a maximum dose of 300 mg once daily as needed to control blood pressure.', 'Avapro (irbesartan) belongs to a group of drugs called angiotensin II receptor blockers. Irbesartan keeps blood vessels from narrowing, which lowers blood pressure and improves blood flow.\r\n\r\nAvapro is used to treat high blood pressure (hypertension). It is sometimes given together with other blood pressure medications.\r\n\r\nAvapro is also used to treat kidney problems caused by type 2 diabetes.\r\n\r\nAvapro may also be used for purposes not listed in this medication guide.'),
(12, 'Dexilant', 'Oral Capsule', 'One 60 mg capsule once daily.\r\nUp to 8 weeks.', 'Dexilant is a proton pump inhibitor that decreases the amount of acid produced in the stomach.\r\n\r\nDexilant is used to treat heartburn caused by gastroesophageal reflux disease (GERD), and to heal erosive esophagitis (damage to the esophagus from stomach acid).\r\n'),
(13, 'Flagyl', 'Oral Tablet', 'One-day treatment – two grams of FLAGYL, given either as a single dose or in two divided doses of one gram each, given in the same day.', 'Flagyl (metronidazole) is an antibiotic that fights bacteria.\r\n\r\nFlagyl is used to treat bacterial infections of the vagina, stomach, liver, skin, joints, brain, and respiratory tract.\r\n\r\nFlagyl will not treat a vaginal yeast infection.'),
(14, 'Percocet', 'Oral Tablet', 'Initiate treatment with PERCOCET tablets 2.5 mg/325 mg adult dosage, with one or 2 tablets every 6 hours as needed for pain. The total daily dose of acetaminophen should not exceed 4 grams.', 'Percocet contains a combination of acetaminophen and oxycodone. Oxycodone is an opioid pain medication. An opioid is sometimes called a narcotic. Acetaminophen is a less potent pain reliever that increases the effects of oxycodone.\r\n\r\nPercocet is used to relieve moderate to severe pain.\r\n\r\nDue of the risks of addiction, abuse, and misuse, even at recommended doses, Percocet is only prescribed when treatment with non-opioid pain relieving medication has not been tolerated or has not provided adequate pain relief.'),
(15, 'Keflex', 'Oral Capsule', 'The usual dose of oral KEFLEX is 250 mg every 6 hours, but a dose of 500 mg every 12 hours may be administered. Treatment is administered for 7 to 14 days.', 'Keflex (cephalexin) is a cephalosporin (SEF a low spor in) antibiotic. It works by fighting bacteria in your body.\r\n\r\nKeflex is used to treat infections caused by bacteria, including upper respiratory infections, ear infections, skin infections, urinary tract infections, and bone infections.\r\n\r\nKeflex may also be used for purposes not listed in this medication guide.'),
(16, 'Benadryl', 'Intramuscular or Intravenous Injection', '5 mg/kg/24 hr or 150 mg/m2/24 hr. Maximum daily dosage is 300 mg. Divide into four doses, administered intravenously at a rate generally not exceeding 25 mg/min, or deep intramuscularly.', 'Benadryl (diphenhydramine) is an antihistamine that reduces the effects of natural chemical histamine in the body. Histamine can produce symptoms of sneezing, itching, watery eyes, and runny nose.\r\n\r\nBenadryl is used to treat sneezing, runny nose, watery eyes, hives, skin rash, itching, and other cold or allergy symptoms.\r\n\r\nBenadryl is also used to treat motion sickness, to induce sleep, and to treat certain symptoms of Parkinson\'s disease.'),
(17, 'Acrivastine', 'Capsules', 'Acrivastine come in 8mg capsules. One capsules three times a day is the recommended dosage', 'Acrivastine is an antihistamine medicine that relieves the symptoms of allergies. It\'s used to treat hay fever, conjunctivitis (red, itchy eyes), eczema and hives (urticaria). It\'s also used for reactions to insect bites and stings and for some food allergies. Acrivastine is known as a non-drowsy antihistamine. It\'s less likely to make you feel sleepy than some other antihistamines. Acrivastine is available on prescription. You can also buy it from pharmacies and supermarkets. It comes as capsules. Sometimes it\'s combined with a decongestant called pseudoephedrine to unblock your nose and sinuses.'),
(18, 'Aspirin (low dose)', 'Standard Tablets\r\nSoluble Tablets\r\nEnteric coated tablets', 'Usual to take a dose of 75mg once a day', 'Daily low-dose aspirin is a blood thinning medicine. Aspirin is also known as acetylsalicylic acid. Low-dose aspirin helps to prevent heart attacks and strokes in people at high risk of them.'),
(19, 'Atenolol', 'Oral Tablet', 'Initial dose: 50 mg orally once a day\r\nMaintenance dose: 50 to 100 mg orally once a day\r\nMaximum dose: 100 mg per day', 'Atenolol (Tenormin) is a beta-blocker that affects the heart and circulation (blood flow through arteries and veins). Atenolol is used to treat angina (chest pain) and hypertension (high blood pressure). Atenolol is also used to lower the risk of death after a heart attack.'),
(20, 'Codeine', 'Tablet form\r\nOral Solution', 'Take codeine exactly as prescribed by your doctor. Follow all directions on your prescription label. Codeine can slow or stop your breathing. Never use this medicine in larger amounts, or for longer than prescribed. Tell your doctor if the medicine seems to stop working as well in relieving your pain.', 'Codeine is an opioid pain medication, sometimes called a narcotic.\r\nCodeine is used to treat mild to moderately severe pain.\r\nCodeine may also be used for purposes not listed in this medication guide.'),
(21, 'Valtrex', 'Tablet or Capsule', 'The recommended dosage of VALTREX for treatment of cold sores is 2 grams twice daily for 1 day taken 12 hours apart. Therapy should be initiated at the earliest symptom of a cold sore (e.g., tingling, itching, or burning).', 'Valtrex (valacyclovir) is an antiviral drug. It slows the growth and spread of the herpes virus to help the body fight the infection.\r\n\r\nValtrex is used to treat infections caused by herpes viruses, including genital herpes, cold sores, and shingles (herpes zoster) in adults.'),
(22, 'Forteo', 'Injection', 'The recommended dose is 20 mcg subcutaneously once a day.', 'Forteo (teriparatide) is a man-made form of parathyroid hormone that exists naturally in the body. Teriparatide increases bone mineral density and bone strength, which may prevent fractures.\r\n\r\nForteo is used to treat osteoporosis caused by menopause, steroid use, or gonadal failure.\r\n\r\nForteo is for use when you have a high risk of bone fracture due to osteoporosis.'),
(23, 'Ofev', 'Tablets or Capsules', 'The recommended dosage of OFEV is 150 mg twice daily administered approximately 12 hours apart.\r\n', 'Ofev (nintedanib) is used in people with diseases that cause scar tissue to form deep within the lungs. The scar tissue thickens and becomes stiff over time, which can make it harder for your lungs to work. Decreased lung function can make it hard for you to breathe. Other medical problems can occur when your brain, heart, and other organs do not get enough oxygen.\r\n\r\nOfev is used to treat idiopathic pulmonary fibrosis (IPF). This medicine is also used to slow the decline in lung function in people with a disorder called systemic sclerosis-associated interstitial lung disease (sometimes called scleroderma-associated ILD).'),
(24, 'Cyotic', 'Liquid', 'Vary from person to person', 'It is used to treat ear infections.'),
(25, 'Scandonest', 'Injection', 'Vary from person to person', 'Scandonest is an anesthetic (numbing medicine) that blocks the nerve impulses that send pain signals to your brain.\r\n\r\nScandonest is used as a local (in only one area) anesthetic for an epidural or spinal block. It is also used as an anesthetic for dental procedures.'),
(26, 'Klonopin', 'Tablets', 'The initial dose for adults with seizure disorders should not exceed 1.5 mg/day divided into three doses. Dosage may be increased in increments of 0.5 to 1 mg every 3 days until seizures are adequately controlled or until side effects preclude any further increase. Maintenance dosage must be individualized for each patient depending upon response. Maximum recommended daily dose is 20 mg.', 'Klonopin (clonazepam) is a benzodiazepine. Clonazepam affects chemicals in the brain that may be unbalanced. Clonazepam is also a seizure medicine, also called an anti-epileptic drug.\r\n\r\nKlonopin is used to treat certain seizure disorders (including absence seizures or Lennox-Gastaut syndrome) in adults and children.\r\n\r\nKlonopin is also used to treat panic disorder (including agoraphobia) in adults.'),
(27, 'Warfarin', 'Tablets', 'Initial dose: 2 to 5 mg orally once a day\r\nMaintenance dose: 2 to 10 mg orally once a day', 'Warfarin is an anticoagulant (blood thinner). Warfarin reduces the formation of blood clots.\r\n\r\nWarfarin is used to treat or prevent blood clots in veins or arteries, which can reduce the risk of stroke, heart attack, or other serious conditions.\r\n\r\nWarfarin may also be used for purposes not listed in this medication guide.'),
(28, 'Bumex', 'Tablets', 'The usual total daily dosage of Bumex tablets is 0.5 mg to 2 mg and in most patients is given as a single dose.\r\n\r\nIf the diuretic response to an initial dose of Bumex tablets is not adequate, in view of its rapid onset and short duration of action, a second or third dose may be given at 4- to 5-hour intervals up to a maximum daily dose of 10 mg. An intermittent dose schedule, whereby Bumex tablets are given on alternate days or for 3 to 4 days with rest periods of 1 to 2 days in between, is recommended as the safest and most effective method for the continued control of edema. In patients with hepatic failure, keep the dosage to a minimum.', 'Bumex is diuretic that is used to treat fluid retention (edema) in people with congestive heart failure, liver disease, or a kidney disorder such as nephrotic syndrome.\r\n\r\nBumex may also be used for purposes not listed in this medication guide.'),
(29, 'Gaviscon', 'Liquid Solution Oral / Tablets', 'ORAL SUSPENSION:\r\nAluminum hydroxide 95 mg-magnesium carbonate 358 mg/15 mL: 15 to 30 mL orally up to 4 times a day after meals and at bedtime as needed or as directed\r\n-Maximum dose: 120 mL/day\r\n\r\nAluminum hydroxide 254 mg-magnesium carbonate 237.5 mg/5 mL: 10 to 20 mL orally up to 4 times a day after meals and at bedtime as needed or as directed\r\n-Maximum dose: 80 mL/day\r\n\r\nTABLETS:\r\nAluminum hydroxide 160 mg-magnesium carbonate 105 mg: 2 to 4 tablets orally up to 4 times a day after meals and at bedtime as needed or as directed\r\n-Maximum dose: 16 tablets/day\r\n\r\nDuration of therapy: Up to 2 weeks\r\n\r\nComment: Patients should avoid swallowing tablets whole.', 'It is used to treat heartburn and upset stomach.\r\nIt may be given to you for other reasons.'),
(30, 'Mobic', 'Tablets', 'Osteoarthritis\r\nFor the relief of the signs and symptoms of osteoarthritis the recommended starting and maintenance oral dose of MOBIC is 7.5 mg once daily. Some patients may receive additional benefit by increasing the dose to 15 mg once daily.\r\n\r\nRheumatoid Arthritis\r\nFor the relief of the signs and symptoms of rheumatoid arthritis, the recommended starting and maintenance oral dose of MOBIC is 7.5 mg once daily. Some patients may receive additional benefit by increasing the dose to 15 mg once daily.', 'Mobic (meloxicam) is a nonsteroidal anti-inflammatory drug (NSAID). Meloxicam works by reducing hormones that cause inflammation and pain in the body.\r\n\r\nMobic is used to treat pain or inflammation caused by rheumatoid arthritis and osteoarthritis in adults. Mobic is also used to treat juvenile rheumatoid arthritis in children who are at least 2 years old.'),
(31, 'Ampicillin', 'Capsule', 'The manufacturer gives no specific dosing instructions.', 'Ampicillin is a penicillin antibiotic that is used to treat or prevent many different types of infections such as bladder infections, pneumonia, gonorrhea, meningitis, or infections of the stomach or intestines.'),
(32, 'Loratadine', 'Tablets', 'Usual Adult Dose for Allergic Rhinitis\r\n10 mg orally once a day\r\n-Maximum dose: 10 mg/day\r\n\r\nUse: Temporary relief of symptoms associated with hay fever or other upper respiratory allergies (e.g., runny nose, itchy, watery eyes, sneezing, itching of the nose/throat)', 'Loratadine is an antihistamine that reduces the effects of natural chemical histamine in the body. Histamine can produce symptoms of sneezing, itching, watery eyes, and runny nose.\r\n\r\nLoratadine is used to treat sneezing, runny nose, watery eyes, hives, skin rash, itching, and other cold or allergy symptoms.\r\n\r\nLoratadine is also used to treat skin hives and itching in people with chronic skin reactions.'),
(33, 'Pantoprazole', 'Tablets', 'Usual Adult Dose for Erosive Esophagitis\r\nTreatment: 40 mg orally once a day\r\n-Duration of therapy: 8 weeks\r\n\r\nMaintenance: 40 mg orally once daily', 'Pantoprazole is a proton pump inhibitor that decreases the amount of acid produced in the stomach.\r\n\r\nPantoprazole is used to treat erosive esophagitis (damage to the esophagus from stomach acid caused by gastroesophageal reflux disease, or GERD) in adults and children who are at least 5 years old. Pantoprazole is usually given for up to 8 weeks at a time while your esophagus heals.\r\n\r\nPantoprazole is also used to treat Zollinger-Ellison syndrome and other conditions involving excess stomach acid'),
(34, 'Diovan', 'Tablets or Capsules', 'The recommended starting dose of Diovan is 80 mg once daily when used as monotherapy in patients who are not volume-depleted. Diovan may be used over a dose range of 80 mg to 320 mg daily, administered once-a-day.', 'Diovan (valsartan) is an type of blood pressure medication called an ARB (angiotensin II receptor antagonist). Diovan keeps blood vessels from narrowing, which lowers blood pressure and improves blood flow.\r\n\r\nDiovan is used to treat high blood pressure (hypertension) in adults and children who are at least 6 years old. It is sometimes given together with other blood pressure medications.\r\n\r\nDiovan is also used in adults to treat heart failure, and to lower the risk of death after a heart attack.'),
(35, 'Truvada', 'Capsule', 'Varis from patient to patient ', 'Truvada contains a combination of emtricitabine and tenofovir disoproxil fumarate. Emtricitabine and tenofovir disoproxil fumarate are antiviral medicines that prevent human immunodeficiency virus (HIV) from multiplying in your body.\r\n\r\nEmtricitabine and tenofovir is to treat HIV, the virus that can cause acquired immunodeficiency syndrome (AIDS). This medicine is not a cure for HIV or AIDS, but it can be used to treat HIV in adults and children who are at least 12 years old and weigh at least 17 kilograms (37 pounds).'),
(36, 'Tocilizumab', 'Tablet or Capsule', '4mg/day', 'Tocilizumab is used to treat moderate to severe rheumatoid arthritis in adults after at least one other medication did not work or has stopped working.\r\n\r\nTocilizumab is also used in adults to treat giant cell arteritis, or inflammation of the lining of your arteries (blood vessels that carry blood from your heart to other parts of your body).\r\n\r\nTocilizumab is used to treat polyarticular juvenile idiopathic arthritis in adults and children ages 2 and older.\r\n\r\nTocilizumab is also used to treat systemic juvenile idiopathic arthritis (or \"Still disease\") in adults and children ages 2 and older.'),
(37, 'Yervoy', 'Tablet or Capsule', 'Varies from patient to patient ', 'Yervoy (ipilimumab) is a cancer medicine that interferes with the growth and spread of cancer cells in the body.\r\n\r\nYervoy is used to treat melanoma (skin cancer) that cannot be treated with surgery or has spread to other parts of the body. It is also used to prevent melanoma from coming back after surgery, including lymph node removal surgery.\r\n\r\nYervoy is also used to treat kidney cancer, sometimes given with another medicine called nivolumab (Opdivo).\r\n\r\nYervoy is also used to treat colorectal cancer that has spread to other parts of the body, that has certain specific DNA mutations, and that has not responded to chemotherapy with other medicines.\r\n\r\nYervoy may also be used for purposes not listed in this medication guide.');

-- --------------------------------------------------------

--
-- Table structure for table `saved_hc`
--

CREATE TABLE `saved_hc` (
  `id` int(11) NOT NULL,
  `hc_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `saved_hc`
--

INSERT INTO `saved_hc` (`id`, `hc_id`, `user_id`) VALUES
(7, 8, 6);

-- --------------------------------------------------------

--
-- Table structure for table `saved_medicines`
--

CREATE TABLE `saved_medicines` (
  `id` int(11) NOT NULL,
  `med_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `saved_medicines`
--

INSERT INTO `saved_medicines` (`id`, `med_id`, `user_id`) VALUES
(6, 18, 7);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `health_conditions`
--
ALTER TABLE `health_conditions`
  ADD PRIMARY KEY (`HC_id`);

--
-- Indexes for table `medicines`
--
ALTER TABLE `medicines`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `saved_hc`
--
ALTER TABLE `saved_hc`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `saved_medicines`
--
ALTER TABLE `saved_medicines`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `health_conditions`
--
ALTER TABLE `health_conditions`
  MODIFY `HC_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `medicines`
--
ALTER TABLE `medicines`
  MODIFY `Id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `saved_hc`
--
ALTER TABLE `saved_hc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `saved_medicines`
--
ALTER TABLE `saved_medicines`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
