<?php include 'header.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Edit Med</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
<link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/animate.css">
<link rel="stylesheet" type="text/css" href="styles/about.css">
<link rel="stylesheet" type="text/css" href="styles/about_responsive.css">
</head>
<body>

<div class="super_container">

	<!-- Menu -->

	<div class="menu trans_500">
		<div class="menu_content d-flex flex-column align-items-center justify-content-center text-center">
			<div class="menu_close_container"><div class="menu_close"></div></div>
			<ul>
				<li class="menu_item"><a href="adminHome.php">Home</a></li>
				<li class="menu_item"><a href="adminHCList.php">Health Conditions</a></li>
				<li class="menu_item"><a href="adminMedList.php">Medicines</a></li>
				<li class="menu_item"><a href="addHC.php">Add Health Conditions</a></li>
				<li class="menu_item"><a href="addMed.php">Add Medicines</a></li>	
				<li class="menu_item"><a href="includes/logout.inc.php">Logout</a></li>	
			</ul>
		</div>
		<div class="menu_social">
			<ul>
				<li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-behance" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
			</ul>
		</div>
	</div>
	
	<!-- Home -->

	<div class="home">
		<!-- <div class="background_image" style="background-image:url(images/about.jpg)"></div> -->
		<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="images/about.jpg" data-speed="0.8"></div>

		<!-- Header -->

		<header class="header" id="header">
			<div>
				<div class="header_top">
					<div class="container">
						<div class="row">
							<div class="col">
								<div class="header_top_content d-flex flex-row align-items-center justify-content-start">
									<div class="logo">
										<a href="index.php"><span>MEDLIB</span></a>	
									</div>
									
									
									<div class="hamburger ml-auto"><i class="fa fa-bars" aria-hidden="true"></i></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="header_nav" id="header_nav_pin">
					<div class="header_nav_inner">
						<div class="header_nav_container">
							<div class="container">
								<div class="row">
									<div class="col">
										<div class="header_nav_content d-flex flex-row align-items-center justify-content-start">
											<nav class="main_nav">
												<ul class="d-flex flex-row align-items-center justify-content-start">
													<li><a href="adminHome.php">Home</a></li>
													<li><a href="adminHCList.php">Health Conditions</a></li>
													<li><a href="adminMedList.php">Medicines</a></li>
													<li><a href="addHC.php">Add Health Conditions</a></li>
													<li><a href="addMed.php">Add Medicines</a></li>
													<li><a href="includes/logout.inc.php">Logout</a></li>
												</ul>
											</nav>
											
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</header>

		<div class="home_container">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="home_content">
							<div class="home_title">Medicine</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- About -->

	<div class="about" style="background-color: #CEF2E8">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 footer_col"></div>
				<div class="col-lg-7 footer_col">
					<div class="footer_contact">
						<div class="footer_contact_title" style="font-size: 24px;color: black;font-weight: bold">Edit Details</div>
						<?php    
						global $conn;
						$id = mysqli_real_escape_string($conn, $_GET['id']);
				        $sql = "SELECT * FROM medicines WHERE Id='$id'";
				        $result = mysqli_query($conn, $sql);
				        $queryResults = mysqli_num_rows($result);
				        if ($queryResults > 0) {
				            if($row = mysqli_fetch_assoc($result)) {
				         	?>
						<div class="footer_contact_form_container">
							<form action="editMedCode.php" class="footer_contact_form" id="footer_contact_form" method="POST">
								<input type="hidden" value="<?php echo $id; ?>" name="id">
								<input type="text" class="footer_contact_input" placeholder="Name of Medicine" name="medname" required="required" value="<?php echo $row['D_Name']; ?>">
								<input type="text" class="footer_contact_input" placeholder="Form of Medicine" name="form" required="required" value="<?php echo $row['D_Type']; ?>">
								<textarea class="footer_contact_input footer_contact_textarea" name="dosage" placeholder="Dosage" required="required"><?php echo $row['D_Dosage']; ?></textarea>
								<textarea class="footer_contact_input footer_contact_textarea" name="description" placeholder="Description" required="required"><?php echo $row['D_Description']; ?></textarea>
								<button class="footer_contact_button" name="msgButton">Update</button>
							</form>
						</div>
					<?php } } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php  include("footer.php"); ?>
</div>

<script src="js/jquery-3.3.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/greensock/TweenMax.min.js"></script>
<script src="plugins/greensock/TimelineMax.min.js"></script>
<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="plugins/greensock/animation.gsap.min.js"></script>
<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="js/about.js"></script>
</body>
</html>