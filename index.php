<?php include 'header.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>MedLib - Home</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Health medical template project">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
<link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/animate.css">
<link rel="stylesheet" type="text/css" href="styles/main_styles.css">
<link rel="stylesheet" type="text/css" href="styles/responsive.css">
</head>
<body>

<div class="super_container">

	<!-- Menu -->

	<div class="menu trans_500">
		<div class="menu_content d-flex flex-column align-items-center justify-content-center text-center">
			<div class="menu_close_container"><div class="menu_close"></div></div>
			<form action="#" class="menu_search_form">
				<input type="text" class="menu_search_input" placeholder="Search" required="required">
				<button class="menu_search_button"><i class="fa fa-search" aria-hidden="true"></i></button>
			</form>
			<ul>
				<li class="menu_item"><a href="index.php">Home</a></li>
				<li class="menu_item"><a href="nearMe.php">Pharmacy Near Me</a></li>
				<li class="menu_item"><a href="healthConditionList.php">All Health Conditions</a></li>
				<li class="menu_item"><a href="medicineList.php">All Medicines</a></li>
				<li class="menu_item"><a href="aboutUs.php">About Us</a></li>
				<li class="menu_item"><a href="profile.php">Profile</a></li>	
				<li class="menu_item"><a href="includes/logout.inc.php">Logout</a></li>	
			</ul>
		</div>
		<div class="menu_social">
			<ul>
				<li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-behance" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
			</ul>
		</div>
	</div>
	
	<!-- Home -->

	<div class="home">
		<div class="background_image" style="background-image:url(images/3.jpg);"></div>

		<!-- Header -->

			<header class="header" id="header">
			<div>
				<div class="header_top">
					<div class="container">
						<div class="row">
							<div class="col">
								<div class="header_top_content d-flex flex-row align-items-center justify-content-start">
									<div class="logo">
										<a href="index.php"><span>MEDLIB</span></a>	
									</div>									
									<div class="hamburger ml-auto"><i class="fa fa-bars" aria-hidden="true"></i></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="header_nav" id="header_nav_pin">
					<div class="header_nav_inner">
						<div class="header_nav_container">
							<div class="container">
								<div class="row">
									<div class="col">
										<div class="header_nav_content d-flex flex-row align-items-center justify-content-start">
											<nav class="main_nav">
												<ul class="d-flex flex-row align-items-center justify-content-start">
													<li><a href="index.php">Home</a></li>
													<li><a href="healthConditionList.php">All Health Conditions</a></li>
													<li><a href="medicineList.php">All Medicines</a></li>
													<li><a href="nearMe.php">Pharmacy Near Me</a></li>
													<li><a href="profile.php">Profile</a></li>
													<li><a href="aboutUs.php">About Us</a></li>
													<li><a href="includes/logout.inc.php">Logout</a></li>
												</ul>
											</nav>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</header>

		<div class="home_container">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="home_content">
							<div class="home_title">Welcome to MedLib</div>
							<div class="cta_title">A Medicine and Drug Search Tool</div>
							<div class="home_text">MedLib is a search tool for pharmaceuticals providing information on Medicine and Drugs used primarily for the UK.</div>
							<div class="button home_button"><a href="aboutUs.php"><span>Read More</span><span>Read More</span></a></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Info Boxes -->

	<div class="info">
		<div class="container">
			<div class="row row-eq-height">

				<!-- Info Box -->
				<div class="col-lg-3 info_box_col">
					
				</div>

				<div class="col-lg-6 info_box_col">
					<div class="info_form_container">
						<div class="info_content">
						<div class="info_form_title">Search</div>
						<form action="searchResult.php" method="POST" class="info_form" id="info_form">
							<input type="text" class="info_input" name="search" placeholder="Search here..." required="required">
							<select name="filter" id="filter" class="info_form_doc info_input info_select">
								<option>Filter By</option>
								<option value="Symptom">Health Condition</option>
								<option value="Medicine">Medicine</option>
							</select>
							<button class="info_form_button" name="submit-search" type="submit">Search</button>
						</form>
					</div>
					</div>
				</div>

				<!-- Info Box -->
				<div class="col-lg-3 info_box_col"></div>
				<!-- Services -->

	<div class="services">
		<div class="container">
			<div class="row">
				<div class="col text-center">
					<div class="section_title">Our Services</div>
					<div class="section_subtitle">to choose from</div>
				</div>
			</div>
			<div class="row icon_boxes_row">
				
				<!-- Icon Box -->
				<div class="col-xl-4 col-lg-6">
					<div class="icon_box">
						<div class="icon_box_title_container d-flex flex-row align-items-center justify-content-start">
							<div class="icon_box_icon"><img src="images/icon_1.svg" alt=""></div>
							<div class="icon_box_title">Medicine</div>
						</div>
						<div class="icon_box_text">You can use our search bar to search a number of medicines to find reliable and accurate information.</div>
					</div>
				</div>

				<!-- Icon Box -->
				<div class="col-xl-4 col-lg-6">
					<div class="icon_box">
						<div class="icon_box_title_container d-flex flex-row align-items-center justify-content-start">
							<div class="icon_box_icon"><img src="images/icon_2.svg" alt=""></div>
							<div class="icon_box_title">Health Conditions</div>
						</div>
						<div class="icon_box_text">You can use the filter option in the search bar to choose to search health conditions. This would give you information on
						symptoms and treatments of health conditions.</div>
					</div>
				</div>

				<!-- Icon Box -->
				<div class="col-xl-4 col-lg-6">
					<div class="icon_box">
						<div class="icon_box_title_container d-flex flex-row align-items-center justify-content-start">
							<div class="icon_box_icon"><img src="images/icon_3.svg" alt=""></div>
							<div class="icon_box_title">Pharmacy Search</div>
						</div>
						<div class="icon_box_text">You can use the pharmacy page to find a pharmacy near you using an interactive map.</div>
					</div>
				</div>

			</div>
			<div class="row">
				<div class="col">
					<div class="button services_button ml-auto mr-auto"><a href="aboutUs.php"><span>read more</span><span>read more</span></a></div>
				</div>
			</div>
		</div>
	</div>

	<!-- Departments -->

				
				
			</div>
		</div>
	</div>


<?php  include("footer.php"); ?>
</div>

<script src="js/jquery-3.3.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="js/custom.js"></script>
</body>
</html>