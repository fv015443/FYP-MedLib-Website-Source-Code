
<!doctype html>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>MedLib - Sign Up</title> <!--Webpage Title-->
	<link href="styles/signup.css" rel="stylesheet" type="text/css">
	<script src="medlib.js"></script>
<!--    this is the search bar text when you click and unclick the search-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  </head>
</head>
<body>
<!-- new nav -->


<div class="login-page">
  <div class="form">
  	<h2>Sign Up</h2>
    <form class="register-formm" action="includes/signup.inc.php" method="post">
	<input type="text" name="uid" placeholder="Enter Username" required="" />
	  <input type="password" name="pwd" placeholder="Enter Password" required="">
	  <input type="password" name="pwd-repeat" placeholder="Retype Password" required="">
	  <input type="text" name="mail" placeholder="Enter Email" required="">
      <button type="submit" name="signup-submit">Sign Up</button>
      <p class="message">Already registered? <a href="login.php">Sign In</a></p>
    </form>
  </div>
</div>
