<?php
session_start();
if (isset($_POST['favButton'])) {

    require 'includes/dbh.inc.php';
    $hc_id = $_POST['hc_id'];
    $user_id = $_SESSION['userId'];
    $type=$_POST['type'];
    if($type==0){
        $sql = "INSERT INTO saved_hc (hc_id,user_id) VALUES (?, ?)";
        $stmt = mysqli_stmt_init($conn);
        if (!mysqli_stmt_prepare($stmt, $sql)) {
            header("Location: ../index.php");
            exit();
        }
        else {
    	   	mysqli_stmt_bind_param($stmt, "ii", $hc_id, $user_id);
    	    mysqli_stmt_execute($stmt);
    	    ?>
    	    <script type="text/javascript">
    	    	alert("Saved to Favourites");
    	    	history.back();
    	    </script>
    	    <?php
    	}
    }else{
        $sql = "DELETE FROM saved_hc WHERE hc_id=? AND user_id=?";
            $stmt = mysqli_stmt_init($conn);
            mysqli_stmt_prepare($stmt, $sql);
            mysqli_stmt_bind_param($stmt, "ii", $hc_id, $user_id);
            mysqli_stmt_execute($stmt);
            ?>
            <script type="text/javascript">
                alert("Removed from Favourites");
                history.back();
            </script>
            <?php
          
    }
}
?>
