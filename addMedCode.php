<?php	
if (isset($_POST['msgButton'])) {
	require 'includes/dbh.inc.php';
	$medname=$_POST['medname'];
	$form=$_POST['form'];
	$description=$_POST['description'];
	$dosage=$_POST['dosage'];
	$sql = "INSERT INTO medicines (D_Name,D_Description, D_Type,D_Dosage) VALUES (?, ?, ?, ?)";
    $stmt = mysqli_stmt_init($conn);
    mysqli_stmt_prepare($stmt, $sql);
    mysqli_stmt_bind_param($stmt, "ssss", $medname, $description, $form,$dosage);
    mysqli_stmt_execute($stmt);
    ?>
    <script type="text/javascript">
        alert("Medicine Added");
        window.location="adminMedList.php";
    </script>
<?php
}
?>