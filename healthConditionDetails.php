<?php include 'header.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Health Condition Details</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
<link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/animate.css">
<link rel="stylesheet" type="text/css" href="styles/news.css">
<link rel="stylesheet" type="text/css" href="styles/news_responsive.css">
</head>
<body>

<div class="super_container">

	<!-- Menu -->

	<div class="menu trans_500">
		<div class="menu_content d-flex flex-column align-items-center justify-content-center text-center">
			<div class="menu_close_container"><div class="menu_close"></div></div>
			<form action="#" class="menu_search_form">
				<input type="text" class="menu_search_input" placeholder="Search" required="required">
				<button class="menu_search_button"><i class="fa fa-search" aria-hidden="true"></i></button>
			</form>
			<ul>
				<li class="menu_item"><a href="index.php">Home</a></li>
		        <li class="menu_item"><a href="nearMe.php">Pharmacy Near Me</a></li>
		        <li class="menu_item"><a href="healthConditionList.php">All Health Conditions</a></li>
		        <li class="menu_item"><a href="medicineList.php">All Medicines</a></li>
		        <li class="menu_item"><a href="aboutUs.php">About Us</a></li>
		        <li class="menu_item"><a href="profile.php">Profile</a></li>  
		        <li class="menu_item"><a href="includes/logout.inc.php">Logout</a></li> 
			</ul>
		</div>
		<div class="menu_social">
			<ul>
				<li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-behance" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
			</ul>
		</div>
	</div>
	
	<!-- Home -->

	<div class="home">
		<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="images/5.jpg" data-speed="0.8"></div>

		<!-- Header -->

		<header class="header" id="header">
			<div>
				<div class="header_top">
					<div class="container">
						<div class="row">
							<div class="col">
								<div class="header_top_content d-flex flex-row align-items-center justify-content-start">
									<div class="logo">
										<a href="#"><span>MEDLIB</span></a>	
									</div>
									
									<div class="hamburger ml-auto"><i class="fa fa-bars" aria-hidden="true"></i></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="header_nav" id="header_nav_pin">
					<div class="header_nav_inner">
						<div class="header_nav_container">
							<div class="container">
								<div class="row">
									<div class="col">
										<div class="header_nav_content d-flex flex-row align-items-center justify-content-start">
											<nav class="main_nav">
												<ul class="d-flex flex-row align-items-center justify-content-start">
													<li><a href="index.php">Home</a></li>
							                        <li><a href="healthConditionList.php">All Health Conditions</a></li>
							                        <li><a href="medicineList.php">All Medicines</a></li>
							                        <li><a href="nearMe.php">Pharmacy Near Me</a></li>
							                        <li><a href="profile.php">Profile</a></li>
							                        <li><a href="aboutUs.php">About Us</a></li>
							                        <li><a href="includes/logout.inc.php">Logout</a></li>
												</ul>
											</nav>
											
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</header>
		<?php
        
		global $conn;
		$id = mysqli_real_escape_string($conn, $_GET['id']);
        $sql = "SELECT * FROM health_conditions WHERE HC_id='$id'";
        $result = mysqli_query($conn, $sql);
        $queryResults = mysqli_num_rows($result);   
        if ($queryResults > 0) {
            if($row = mysqli_fetch_assoc($result)) {
        	?>
				<div class="home_container">
					<div class="container">
						<div class="row">
							<div class="col">
								<div class="home_content">
									<div class="home_title"><?php echo $row['HC_Name']; ?></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		<div class="news">
			<div class="container">
				<div class="row">
					<div class="col-lg-8">
						<div class="news_post_title"><a href="#"><?php echo $row['HC_Name']; ?></a></div>
						<div class="sidebar_categories">
							<div class="categories">
								<div class="news_post_title"><a href="#">Description</a></div>
								<div class="news_post_text">
									<p><?php echo nl2br($row['HC_Descrip']); ?></p>
								</div>
								<div class="news_post_title"><a href="#">Symptoms</a></div>
								<div class="news_post_text">
									<p><?php echo nl2br($row['HC_Symptom']); ?></p>
								</div>
								<div class="news_post_title"><a href="#">Treatments</a></div>
								<div class="news_post_text">
									<p><?php echo nl2br($row['HC_Medicine']); ?></p>
								</div>
								<form method="post" action="saveHC.php">
									<input type="hidden" name="hc_id" value="<?php echo $id;?>">
									<?php
									$user_id=$_SESSION['userId'];
									$sql = "SELECT * FROM saved_hc WHERE user_id='$user_id' and hc_id='$id'";
									$result = mysqli_query($conn, $sql);
									$queryResult = mysqli_num_rows($result);

									if ($queryResult > 0) {
									?>
										<input type="hidden" name="type" value="1">
										<div class="news_post_text"><button type="submit" name="favButton" class="footer_remove_button" >Remove from Favourites</button></div>
									<?php 
									}
									else{?>
										<input type="hidden" name="type" value="0">
										<div class="news_post_text"><button type="submit" name="favButton" class="footer_contact_button" >Add to Favourites</button></div>
									<?php } ?>
								</form>
							</div>
						</div>	
					</div>
				</div>
			</div>
		</div>
	<?php } 
} ?>
			
<!-- Footer -->
<?php include("footer.php"); ?>
</div>

<script src="js/jquery-3.3.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/greensock/TweenMax.min.js"></script>
<script src="plugins/greensock/TimelineMax.min.js"></script>
<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="plugins/greensock/animation.gsap.min.js"></script>
<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="js/news.js"></script>
</body>
</html>