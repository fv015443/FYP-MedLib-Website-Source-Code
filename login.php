<!doctype html>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>MedLib - Login Page</title> <!--Webpage Title-->
	<link href="styles/login.css" rel="stylesheet" type="text/css">

<!--    this is the search bar text when you click and unclick the search-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  </head>
<body style="background-color: white;">


<div class="login-page">
  <div class="form">
  	<h1>Login</h1>
    <form class="login-form" action="includes/login.inc.php" method="post">
    <input type="text" name="mailuid" placeholder="Username/Email..." required="">
    <input type="password" name="pwd" placeholder="Password..." required="">
    <button type="submit" name="login-submit">Login</button>      
    <p class="message">Not registered? <a href="signUp.php">Create an account</a></p>
    </form>
  </div>
</div>
</body>
</html>